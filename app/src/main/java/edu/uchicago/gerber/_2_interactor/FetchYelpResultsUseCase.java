package edu.uchicago.gerber._2_interactor;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import edu.uchicago.gerber.App;
import edu.uchicago.gerber._1_data.core.YelpSearchException;
import edu.uchicago.gerber._1_data.core.RestaurantDirectoryInterface;
import edu.uchicago.gerber._1_data.core.yelpapi.YelpResultsData;

/**
 * The FetchYelpResultsUseCase interacts with a {@link RestaurantDirectoryInterface}
 * to pull requested data from the Yelp API. As with all use cases should only be called in the
 * presenter. Takes a string (location) as an argument and returns an Object of type
 * YelpResultsData.
 */

public class FetchYelpResultsUseCase extends UseCase<String, Void, YelpResultsData> {

    public static int MY_PERMISSIONS_REQUEST_INTERNET =1;
    /**
     * The interface Yelp use case callback.
     */
    public interface YelpUseCaseCallback extends UseCaseCallback<YelpResultsData>{}

    /**
     * Instantiates a new Fetch yelp results use case.
     *
     * @param caseCallback the callback implemented by the class which created this usecase.
     */
    public FetchYelpResultsUseCase(YelpUseCaseCallback caseCallback) {
        super(caseCallback);
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (
                ContextCompat.checkSelfPermission(App.getContext(),
                        Manifest.permission.INTERNET)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(App.getRouter(),
                    Manifest.permission.INTERNET)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(App.getRouter(),
                        new String[]{Manifest.permission.INTERNET},
                        MY_PERMISSIONS_REQUEST_INTERNET);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            // Permission has already been granted
        }
    }

    @Override
    protected YelpResultsData doInBackground(String... params) {
        YelpResultsData yelpResultsData = null;
        try {
            yelpResultsData = App.getRestaurnatDirectory().fetchData(params[0], params[1]);
        } catch (YelpSearchException e) {
            return null;
        }
        return yelpResultsData;
    }

    @Override
    protected void onPostExecute(YelpResultsData yelpResultsData) {

        callback.passPostExecute(yelpResultsData);
    }
}
