package edu.uchicago.gerber._4_view.frags;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import edu.uchicago.gerber._1_data.core.yelpapi.YelpResultsData;
import edu.uchicago.gerber._3_presenter.NewPresenter;
import edu.uchicago.gerber._3_presenter.utils.PrefsMgr;
import edu.uchicago.gerber._4_view.interfaces.NewView;


/**
 * The NewFragment is the fragment displayed when a user is searching for Restaurants. A fairly
 * basic fragment which has an EditText for the city/location to search and a search button.
 *
 * Passes on the {@link YelpResultsData} to the {@link ResultsFragment}.
 */

public class NewFragment extends BaseFragment<NewPresenter> implements NewView {

    private EditText mCityField, mNameField;
    private Button mExtractButton, mClearButton;

    //this is a proxy to our database
    private InputMethodManager mImm;
    private ProgressDialog progressDialog;

    private boolean mArgsPut = false;

    /**
     * The Enter listener for the City EditText.
     */
    EditText.OnKeyListener enterListener = new EditText.OnKeyListener() {

        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                // Perform action on key press

                mImm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                return true;
            }
            return false;
        }
    };


    /**
     * Instantiate a NewFragment. The text was previously used for sharing into the app
     * (e.g. through a yelp link) which is not in the final demo app, but if re-implemented
     * a shared link should be directed to the NewFragment.
     *
     * @param text the text shared (not used in demo app)
     * @return the new fragment
     */
    public static NewFragment newInstance(String text) {
        NewFragment fragment = new NewFragment();
        Bundle args = new Bundle();
        if(text!=null) {
            args.putSerializable("SHARE", text);
        }
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(edu.uchicago.gerber.R.layout.frag_scroll_new, container, false);


        mCityField = (EditText) v.findViewById(edu.uchicago.gerber.R.id.edit_main_city);
        mNameField = (EditText) v.findViewById(edu.uchicago.gerber.R.id.edit_main_name);

        mExtractButton = (Button) v.findViewById(edu.uchicago.gerber.R.id.extract_yelp_button);
        mClearButton = (Button) v.findViewById(edu.uchicago.gerber.R.id.clear_button);


        mImm = (InputMethodManager) getActivity().getSystemService(
                Context.INPUT_METHOD_SERVICE);

        mCityField.setOnKeyListener(enterListener);
        mCityField.setText(PrefsMgr.getString(getContext(), PrefsMgr.CITY));

        mExtractButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                presenter.performSearch(mNameField.getText().toString(), mCityField.getText().toString());

                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(mCityField.getWindowToken(), 0);
            }
        });

        mClearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCityField.setText("");
                mNameField.setText("");
                presenter.clearFields();
            }

        });




        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    protected NewPresenter createPresenter() {
        return new NewPresenter();
    }

    /**
     * Progress dialog displayed when searching for results.
     */
    @Override
    public void startProgress() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("Fetching data");
        progressDialog.setMessage("One moment please...");
        progressDialog.setCancelable(true);

        progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                presenter.cancelSearch();
                progressDialog.dismiss();
            }
        });
        progressDialog.show();
    }

    @Override
    public void stopProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void showToast(String text) {
        Toast.makeText(getContext(), text, Toast.LENGTH_SHORT).show();
    }

    /**
     * Call back to the single Activity that hosts all frags. When .showResultFragment() is shown,
     * then onStart(), onResume() and eventually onStop() will be called
     * @param results the YelpResultsData from a search.
     */
    @Override
    public void displaySearchResults(YelpResultsData results) {

        getRouter().showResultFragment(results);
    }


    @Override
    public void refreshFields() {

        //maybe use the router.
        FragmentTransaction fragTransaction = getFragmentManager().beginTransaction();
        fragTransaction.detach(this);
        fragTransaction.attach(this);
        fragTransaction.commit();

    }
}

