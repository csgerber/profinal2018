package edu.uchicago.gerber._1_data.core.yelpapi;

import java.io.Serializable;

public class BusinessCategory implements Serializable {
    public String alias;
    public String title;
}
