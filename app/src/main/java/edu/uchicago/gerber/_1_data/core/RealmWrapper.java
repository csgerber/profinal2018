package edu.uchicago.gerber._1_data.core;

import edu.uchicago.gerber.App;
import edu.uchicago.gerber._1_data.model.PlaceModel;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;


/**
 * This class is responsible for implementing the abstract logic in {@link RepoInterface} necessary
 * to use Realm as a backend database. Implements the CRUD operations in RepoInterface with
 * Realm-specific instructions. All methods here are overrides of {@link RepoInterface}, so
 * see that file for description of their operation.
 */

public class RealmWrapper implements RepoInterface {

    @Override
    public void create(PlaceModel place) {

        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.copyToRealm(RealmMapper.getPlaceRealm(place));
        realm.commitTransaction();
    }

    @Override
    public PlaceModel read(String id) {
        Realm realm = Realm.getDefaultInstance();
        PlaceRealm place = realm.where(PlaceRealm.class).equalTo("id", id).findFirst();
       return RealmMapper.getPlaceModel(place);
    }

    @Override
    public void update(PlaceModel placeModel) {

        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();

//        PlaceRealm placeRealm = realm.where(PlaceRealm.class).equalTo("id", placeModel.getId()).findFirst();

        realm.copyToRealmOrUpdate(RealmMapper.getPlaceRealm(placeModel));
        realm.commitTransaction();

    }

    @Override
    public void delete(String id) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        PlaceRealm place = realm.where(PlaceRealm.class).equalTo("id", id).findFirst();

        place.deleteFromRealm();
        realm.commitTransaction();


    }


    @Override
    public List<PlaceModel> list() {
        Realm.init(App.getContext());
        Realm realm = Realm.getDefaultInstance();
        RealmResults<PlaceRealm> places = realm.where(PlaceRealm.class)

                .findAllSorted("timestamp", Sort.DESCENDING);
        return  RealmMapper.getPlaceModels(places);

    }

}
