package edu.uchicago.gerber._1_data.core.yelpapi;

import android.os.Build;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import edu.uchicago.gerber._1_data.model.PlaceModel;
import edu.uchicago.gerber._4_view.frags.NewFragment;
import edu.uchicago.gerber._4_view.frags.ResultsFragment;


/**
 * The YelpResultsData class is the result of searches performed in the {@link NewFragment}.
 * These results are passed on to a {@link ResultsFragment}
 * and contain a list of the {@link Business} representation of yelp data (it is only converted
 * into a {@link PlaceModel} after the restaurant
 * has been clicked to save to the database).
 */
public class YelpResultsData implements Serializable {


    /**
     * List of Business objects (basic representation of Yelp data).
     */
    public List<Business> businesses;


    /**
     * Returns a basic string of information for the {@link ResultsFragment}
     * to display. Contains the name, location, and star rating represented as text.
     *
     * @return the simple values
     */
    public ArrayList<String> getSimpleValues() {
        ArrayList<String> simpleValues = new ArrayList<>();

        try {
            for (Business biz : businesses) {

                simpleValues.add(biz.name + " | " + biz.location.address1 + " | " + getStars(biz.rating));
            }
        } catch (Exception e) {
            e.printStackTrace();
            //will continue on its own
        }
        return simpleValues;
    }

    /**
     * Get the Yelp star rating and convert to text.
     * @param rating double containing the star rating as a text description
     * @return string of stars representing rating
     */
    private String getStars(double rating) {

        int nHalfCode = Integer.parseInt("00BD", 16);
        char[] cChars = Character.toChars(nHalfCode);
        String strHalf = String.valueOf(cChars);
        int baseRating = (int)Math.floor(rating);
        boolean hasHalf = (rating - Math.floor(rating)) > 0;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            return String.join("", Collections.nCopies(baseRating, "*")) + (hasHalf ? strHalf : "");
        }
        return String.format("%0" + baseRating + "d", 0).replace("0", "*");
    }


}
