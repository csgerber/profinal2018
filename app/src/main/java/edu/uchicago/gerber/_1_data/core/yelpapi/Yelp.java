package edu.uchicago.gerber._1_data.core.yelpapi;

import android.util.Log;

import com.google.gson.Gson;
import com.yelp.fusion.client.connection.YelpFusionApi;
import com.yelp.fusion.client.connection.YelpFusionApiFactory;
import com.yelp.fusion.client.models.SearchResponse;

import org.scribe.builder.ServiceBuilder;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.oauth.OAuthService;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Response;


/**
 * The Yelp class is a helper class for connecting to the Yelp API. Contains API keys,
 * authentication logic, method for pulling the {@link YelpResultsData}
 */
public class Yelp {


    //Please use your own keys here

    //https://www.yelp.com/developers/v2/manage_api_keys
    private static final String CONSUMER_KEY = "dSZgGbpE51gcJ2mPFy8Dag";
    private static final String CONSUMER_SECRET = "CAe7Yp1NEYVPh2Z2ZpDDetqUpWM";
    private static final String TOKEN = "ksJ-aFEUA-sO8YKI9TwbTem8DoLOOtH0";
    private static final String TOKEN_SECRET = "O1oqDGf93zFEz-_ctYgicO1VYQM";
    // TODO: replace this with the API KEY
    private static final String API_KEY = "FxVS9ym_GgG992_zBLzSvjNubU2PEwA8ERyfEGJKYZVdK7A1EWXRDmr2RHQ3jb3XcdOTTVYaPC_ITHITXDjRjF7hYlEdMcT5hXtNji97JQBAncJn4yeloWeTPFuzXHYx";

    OAuthService service;

    Token accessToken;


    public Yelp(){
        this.service = new ServiceBuilder().provider(YelpApi2.class).apiKey(CONSUMER_KEY).apiSecret(CONSUMER_SECRET).build();
        this.accessToken = new Token(TOKEN, TOKEN_SECRET);
    }


    /**
     * Returns up to 20 results for a given yelp search, returning all the items as one
     * {@link YelpResultsData} object. Connects to the yelp API with keys provided in this class.
     *
     * @param searchTerm the search term
     * @param city       the city, ZIP, etc.
     * @return the yelpresultsdata
     */
    public YelpResultsData searchMultiple(String searchTerm, String city) {

        // Execute a signed call to the Yelp service.
        try {
            YelpFusionApiFactory apiFactory = new YelpFusionApiFactory();
            YelpFusionApi api = apiFactory.createAPI(API_KEY);
            HashMap<String, String> params = new HashMap<>();
            params.put("location", city);
            params.put("term", searchTerm);
            params.put("limit", "20");
            Call<SearchResponse> call = api.getBusinessSearch(params);
            Response<SearchResponse> response = call.execute();

            YelpResultsData mYelpSearchResult = null;
            Log.d("Yelp", "searchMultiple: " + new Gson().toJson(response.body()));

            mYelpSearchResult = new Gson().fromJson(new Gson().toJson(response.body()), YelpResultsData.class);

            return mYelpSearchResult;
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }




}