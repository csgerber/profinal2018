import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:proyelp/search_result.dart';
import 'package:proyelp/yelp_response.dart';

class SearchScreen extends StatefulWidget {
  SearchScreen();
  @override
  SearchScreenState createState() {
    // TODO: implement createState
    return SearchScreenState();
  }
}

class SearchScreenState extends State<SearchScreen> {
  String location = "Chicago";
  String term = "House Pub";
  TextEditingController termController;
  TextEditingController locationController;
  @override
  initState() {
    super.initState();
    termController = TextEditingController(
      text: term,
    );
    termController.addListener(() {
      setState(() {
        term = termController.text;
      });
    });
    locationController = TextEditingController(
      text: location,
    );
    locationController.addListener(() {
      setState(() {
        location = locationController.text;
      });
    });
  }

  Widget termField() {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Container(child: Text("Name")),
      TextFormField(
        controller: termController,
        decoration: InputDecoration(hintText: "e.g. Pleasant Hub"),
      )
    ]);
  }

  Widget locationField() {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Container(child: Text("Location")),
      TextFormField(
        controller: locationController,
        decoration:
            InputDecoration(hintText: "e.g. Bruxelles or Chicago or 98101"),
      )
    ]);
  }

  search() async {
    BusinessesListBean business = await Navigator.push(context,
        CupertinoPageRoute(builder: (BuildContext context) {
      return SearchResult(
        location: location,
        term: term,
      );
    }));
    Navigator.pop(context, business);
  }

  Widget searchButton() {
    return FlatButton(
      color: Colors.blue,
      onPressed: this.search,
      child: Row(
        children: <Widget>[
          Icon(Icons.search),
          Text("Search"),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Title(
          child: Text("Search Places"),
          color: Colors.white,
        ),
      ),
      body: Container(
          padding: EdgeInsets.all(10),
          child: ListView(
            children: <Widget>[
              this.termField(),
              this.locationField(),
              this.searchButton()
            ],
          )),
    );
  }
}
